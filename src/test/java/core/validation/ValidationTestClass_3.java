package core.validation;

import core.commands.validation.annotations.BlankFieldValidator;
import core.commands.validation.annotations.IdValidator;

import static core.operationresult.DefaultMessages.MSG_BLANK_FIELD;
import static core.operationresult.DefaultMessages.MSG_ID_INVALID;

public class ValidationTestClass_3 {

    @BlankFieldValidator(message = MSG_BLANK_FIELD, fieldName = "id")
    @IdValidator(message = MSG_ID_INVALID, fieldName = "id")
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
