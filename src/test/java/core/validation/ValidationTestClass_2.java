package core.validation;

import core.commands.validation.annotations.BlankFieldValidator;
import core.commands.validation.annotations.PasswordValidator;

import static core.operationresult.DefaultMessages.MSG_BLANK_FIELD;
import static core.operationresult.DefaultMessages.MSG_WEAK_PASSWORD;

public class ValidationTestClass_2 {

    @BlankFieldValidator(message = MSG_BLANK_FIELD, fieldName = "password")
    @PasswordValidator(message = MSG_WEAK_PASSWORD, fieldName = "password")
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
