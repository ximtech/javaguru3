package core.validation;

import core.commands.validation.annotations.BlankFieldValidator;
import core.commands.validation.annotations.EmailValidator;

import static core.operationresult.DefaultMessages.MSG_BLANK_FIELD;
import static core.operationresult.DefaultMessages.MSG_EMAIL_INVALID;

public class ValidationTestClass_1 {

    @BlankFieldValidator(message = MSG_BLANK_FIELD, fieldName = "email")
    @EmailValidator(message = MSG_EMAIL_INVALID, fieldName = "email")
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
