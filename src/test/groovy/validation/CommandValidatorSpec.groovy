package validation

import core.validation.ValidationTestClass_1
import core.validation.ValidationTestClass_2
import core.validation.ValidationTestClass_3
import core.commands.validation.CommandValidator
import core.exceptions.ValidationException
import core.operationresult.DefaultMessages
import core.operationresult.Status
import core.operationresult.Type
import spock.lang.Specification

class CommandValidatorSpec extends Specification {

    ValidationTestClass_1 validationTestClass_1
    ValidationTestClass_2 validationTestClass_2
    ValidationTestClass_3 validationTestClass_3

    void setup() {
        validationTestClass_1 = new ValidationTestClass_1()
        validationTestClass_2 = new ValidationTestClass_2()
        validationTestClass_3 = new ValidationTestClass_3()
    }

    void "test case 1: email validation test"() {
        when:
        CommandValidator.validateCommand(validationTestClass_1)

        then:
        ValidationException exception_1 = thrown(ValidationException)
        validateExceptionResponse(exception_1)
        exception_1.responseMessage.message == String.format(DefaultMessages.MSG_BLANK_FIELD, exception_1.responseMessage.fieldName)

        when:
        validationTestClass_1.setEmail("notValidMail.com")
        CommandValidator.validateCommand(validationTestClass_1)

        then:
        ValidationException exception_2 = thrown(ValidationException)
        validateExceptionResponse(exception_2)
        exception_2.responseMessage.message == DefaultMessages.MSG_EMAIL_INVALID

        when:
        validationTestClass_1.setEmail("valid@mail.com")
        CommandValidator.validateCommand(validationTestClass_1)

        then:
        notThrown(ValidationException)
    }

    void "test case 2: password validation test"() {
        when:
        CommandValidator.validateCommand(validationTestClass_2)

        then:
        ValidationException exception_1 = thrown(ValidationException)
        validateExceptionResponse(exception_1)
        exception_1.responseMessage.message == String.format(DefaultMessages.MSG_BLANK_FIELD, exception_1.responseMessage.fieldName)

        when:
        validationTestClass_2.setPassword("weakPassword")
        CommandValidator.validateCommand(validationTestClass_2)

        then:
        ValidationException exception_2 = thrown(ValidationException)
        validateExceptionResponse(exception_2)
        exception_2.responseMessage.message == DefaultMessages.MSG_WEAK_PASSWORD

        when:
        validationTestClass_2.setPassword("3120456AAbcd")
        CommandValidator.validateCommand(validationTestClass_2)

        then:
        notThrown(ValidationException)
    }

    void "test case 3: id validation test"() {
        when:
        CommandValidator.validateCommand(validationTestClass_3)

        then:
        ValidationException exception_1 = thrown(ValidationException)
        validateExceptionResponse(exception_1)
        exception_1.responseMessage.message == String.format(DefaultMessages.MSG_BLANK_FIELD, exception_1.responseMessage.fieldName)

        when:
        validationTestClass_3.setId(-1L)
        CommandValidator.validateCommand(validationTestClass_3)

        then:
        ValidationException exception_2 = thrown(ValidationException)
        validateExceptionResponse(exception_2)
        exception_2.responseMessage.message == DefaultMessages.MSG_ID_INVALID

        when:
        validationTestClass_3.setId(1L)
        CommandValidator.validateCommand(validationTestClass_3)

        then:
        notThrown(ValidationException)
    }

    private static boolean validateExceptionResponse(ValidationException exception) {
        boolean isTypeCorrect = exception.responseMessage.type == Type.ERROR
        boolean isStatusCorrect = exception.responseMessage.status == Status.UNPROCESSABLE_ENTITY
        return isTypeCorrect && isStatusCorrect
    }

}
