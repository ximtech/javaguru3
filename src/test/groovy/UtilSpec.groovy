import core.database.UserRole
import core.database.entities.User
import core.dto.UserDTO
import core.dto.utils.DTOMappingUtils
import spock.lang.Specification

class UtilSpec extends Specification {

    void "test user entity mapping to dto"() {
        given:
        User user = new User(false, false, true, "3120456abcd", "test@mail.com", "", "Test", "User", UserRole.ROLE_USER)

        when:
        UserDTO dto = DTOMappingUtils.constructUserDtoFromEntity(user)

        then:
        !dto.accountLocked
        !dto.deleted
        dto.enabled
        dto.password == "3120456abcd"
        dto.emailAddress == "test@mail.com"
        dto.firstName == "Test"
        dto.lastName == "User"
        dto.role == UserRole.ROLE_USER
    }
}
