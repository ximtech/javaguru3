package core.mq.interfaces.mail;

public interface EmailProducer {

    void sendWelcomeMailToUser(String firstName, String lastName, String email);
}
