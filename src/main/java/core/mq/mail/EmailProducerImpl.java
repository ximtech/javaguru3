package core.mq.mail;

import core.commands.logging.DataLogger;
import core.commands.mail.SendWelcomeMailCommand;
import core.mq.interfaces.mail.EmailProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import static core.ActiveMQConfig.MESSAGE_QUEUE;

@Service
public class EmailProducerImpl implements EmailProducer {

    private JmsTemplate jmsTemplate;

    @Autowired
    public EmailProducerImpl(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @Override
    public void sendWelcomeMailToUser(String firstName, String lastName, String email) {
        SendWelcomeMailCommand command = new SendWelcomeMailCommand(firstName, lastName, email);
        DataLogger.logMessageSendingToQueue(command, MESSAGE_QUEUE);
        jmsTemplate.convertAndSend(MESSAGE_QUEUE, command);
    }
}
