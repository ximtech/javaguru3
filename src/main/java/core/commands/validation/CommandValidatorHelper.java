package core.commands.validation;

import core.exceptions.ValidationException;
import core.operationresult.ResponseMessage;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.lang.reflect.Array;
import java.util.Collection;

class CommandValidatorHelper {
    private static final String VALIDATE_EMAIL_PATTERN = "^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)+$";
    private static final String VALIDATE_PASSWORD_STRENGTH_PATTERN = "\\A(?=\\S*?[0-9])(?=\\S*?[a-z])(?=\\S*?[A-Z])\\S{8,}\\z";

    static void validateId(Long value, String message, String fieldName) {
        if (value < 0) {
            throw new ValidationException(ResponseMessage.businessError(message, fieldName));
        }
    }

    static void validateUsername(String value, String message, String fieldName) {
        validateEmail(value, message, fieldName);
    }

    static void validateEmail(String value, String message, String fieldName) {
        if (!isValidEmail(value.trim())) {
            throw new ValidationException(ResponseMessage.businessError(message, fieldName));
        }
    }

    static void validatePassword(String value, String message, String fieldName) {
        if (!isPasswordValid(value)) {
            throw new ValidationException(ResponseMessage.businessError(message, fieldName));
        }
    }

    static void validateNullability(Object value, String message, String fieldName) {
        if (value == null || isBlankString(value) || isEmptyCollection(value) || isEmptyArray(value) || isEmptyMultipartFile(value)) {
            throw new ValidationException(ResponseMessage.businessError(message, fieldName));
        }
    }

    private static boolean isValidEmail(String email) {
        return email.matches(VALIDATE_EMAIL_PATTERN);
    }

    private static boolean isPasswordValid(String password) {
        return password.matches(VALIDATE_PASSWORD_STRENGTH_PATTERN);
    }

    private static boolean isBlankString(Object object) {
        return object instanceof String && StringUtils.isBlank((String) object);
    }

    private static boolean isEmptyCollection(Object object) {
        return object instanceof Collection && ((Collection) object).isEmpty();
    }

    private static boolean isEmptyArray(Object value) {
        return value.getClass().isArray() && Array.getLength(value) == 0;
    }

    private static boolean isEmptyMultipartFile(Object value) {
        return value instanceof MultipartFile && ((MultipartFile) value).isEmpty();
    }
}
