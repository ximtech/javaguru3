package core.commands.validation;

import core.commands.validation.annotations.*;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CommandValidator {
    private static final Map<Class<?>, ValidatorType> verifiersMap;

    static {
        verifiersMap = new HashMap<>();
        verifiersMap.put(BlankFieldValidator.class, ValidatorType.BLANK_FIELD_VALIDATOR);
        verifiersMap.put(EmailValidator.class, ValidatorType.EMAIL_VALIDATOR);
        verifiersMap.put(IdValidator.class, ValidatorType.ID_VALIDATOR);
        verifiersMap.put(UserNameValidator.class, ValidatorType.USER_NAME_VALIDATOR);
        verifiersMap.put(PasswordValidator.class, ValidatorType.PASSWORD_VALIDATOR);
    }

    enum ValidatorType {
        BLANK_FIELD_VALIDATOR, EMAIL_VALIDATOR, ID_VALIDATOR, USER_NAME_VALIDATOR, PASSWORD_VALIDATOR
    }

    public static void validateCommand(Object request) {
        Field[] fields = request.getClass().getDeclaredFields();
        Arrays.stream(fields).forEach((Field field) -> {
                    Annotation[] fieldAnnotations = field.getDeclaredAnnotations();
                    validateCommandParameters(request, fieldAnnotations, field); });
    }

    private static void validateCommandParameters(Object request,
                                                  Annotation[] fieldAnnotations,
                                                  Field field) {
        Arrays.stream(fieldAnnotations).forEach((Annotation annotation) -> {
            ValidatorType validatorType = verifiersMap.get(annotation.annotationType());
            if (validatorType != null) {
                resolveAnnotation(validatorType, annotation, request, field);
            } });
    }

    private static void resolveAnnotation(ValidatorType validatorType,
                                          Annotation annotation,
                                          Object request,
                                          Field field) {
        Class<?> beanClass = request.getClass();
        Object value = getFieldValue(request, field, beanClass);

        switch (validatorType) {
            case BLANK_FIELD_VALIDATOR:
                BlankFieldValidator blankFieldValidator = (BlankFieldValidator) annotation;
                CommandValidatorHelper.validateNullability(value, blankFieldValidator.message(), blankFieldValidator.fieldName());
                break;
            case EMAIL_VALIDATOR:
                EmailValidator emailValidator = (EmailValidator) annotation;
                CommandValidatorHelper.validateEmail((String) value, emailValidator.message(), emailValidator.fieldName());
                break;
            case ID_VALIDATOR:
                IdValidator idValidator = (IdValidator) annotation;
                CommandValidatorHelper.validateId((Long) value, idValidator.message(), idValidator.fieldName());
                break;
            case USER_NAME_VALIDATOR:
                UserNameValidator userNameValidator = (UserNameValidator) annotation;
                CommandValidatorHelper.validateUsername((String) value, userNameValidator.message(), userNameValidator.fieldName());
                break;
            case PASSWORD_VALIDATOR:
                PasswordValidator passwordValidator = (PasswordValidator) annotation;
                CommandValidatorHelper.validatePassword((String) value, passwordValidator.message(), passwordValidator.fieldName());
                break;
            default:
                throw new IllegalArgumentException("Unknown validator type");
        }
    }

    private static Object getFieldValue(Object request, Field field, Class<?> beanClass) {
        try {
            return new PropertyDescriptor(field.getName(), beanClass).getReadMethod().invoke(request);
        } catch (IllegalAccessException | InvocationTargetException | IntrospectionException e) {
            throw new RuntimeException(e);
        }
    }
}
