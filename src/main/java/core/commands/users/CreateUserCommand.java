package core.commands.users;

import core.commands.DomainCommand;
import core.commands.VoidResult;
import core.commands.validation.annotations.BlankFieldValidator;
import core.commands.validation.annotations.EmailValidator;
import core.commands.validation.annotations.PasswordValidator;

import java.util.StringJoiner;

import static core.operationresult.DefaultMessages.*;

public class CreateUserCommand implements DomainCommand<VoidResult> {

    @BlankFieldValidator(message = MSG_BLANK_FIELD, fieldName = FIELD_ID_FIRST_NAME)
    private String firstName;

    @BlankFieldValidator(message = MSG_BLANK_FIELD, fieldName = FIELD_ID_LAST_NAME)
    private String lastName;

    @BlankFieldValidator(message = MSG_BLANK_FIELD, fieldName = FIELD_ID_PASSWORD)
    @PasswordValidator(message = MSG_WEAK_PASSWORD, fieldName = FIELD_ID_PASSWORD)
    private String password;

    @BlankFieldValidator(message = MSG_BLANK_FIELD, fieldName = FIELD_ID_EMAIL)
    @EmailValidator(message = MSG_EMAIL_INVALID)
    private String email;

    public CreateUserCommand() {}

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", "CreateUserCommand {", "}")
                .add("firstName='" + firstName + "'")
                .add("lastName='" + lastName + "'")
                .add("password='" + password + "'")
                .add("email='" + email + "'")
                .toString();
    }
}
