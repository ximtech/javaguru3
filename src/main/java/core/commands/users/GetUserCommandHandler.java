package core.commands.users;

import core.services.interfaces.clients.UserService;
import core.services.interfaces.command.DomainCommandHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GetUserCommandHandler implements DomainCommandHandler<GetUserCommand, GetUserResult> {

    private UserService clientService;

    @Autowired
    public GetUserCommandHandler(UserService clientService) {
        this.clientService = clientService;
    }

    @Override
    public GetUserResult execute(GetUserCommand command) {
        return clientService.getUser(command);
    }

    @Override
    public Class getCommandType() {
        return GetUserCommand.class;
    }
}
