package core.commands.users;

import com.fasterxml.jackson.annotation.JsonInclude;
import core.dto.UserDTO;
import core.operationresult.ProcessingResult;

public class GetUserResult extends ProcessingResult {

    @JsonInclude(JsonInclude.Include.NON_NULL)
	private UserDTO user;

    public GetUserResult() {
    }

    public GetUserResult(UserDTO user) {
		this.user = user;
	}

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public UserDTO getUser() {
		return user;
	}

}
