package core.commands.users;

import core.commands.DomainCommand;
import core.commands.validation.annotations.BlankFieldValidator;
import core.commands.validation.annotations.IdValidator;

import java.util.StringJoiner;

import static core.operationresult.DefaultMessages.FIELD_ID_ID;
import static core.operationresult.DefaultMessages.MSG_BLANK_FIELD;
import static core.operationresult.DefaultMessages.MSG_ID_INVALID;

public class GetUserCommand implements DomainCommand<GetUserResult> {

    @BlankFieldValidator(message = MSG_BLANK_FIELD, fieldName = FIELD_ID_ID)
    @IdValidator(message = MSG_ID_INVALID, fieldName = FIELD_ID_ID)
    private Long id;     // user id

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", "GetUserCommand {", "}")
                .add("id=" + id)
                .toString();
    }
}
