package core.commands.users;

import core.commands.VoidResult;
import core.services.interfaces.clients.UserService;
import core.services.interfaces.command.DomainCommandHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
class CreateUserCommandHandler implements DomainCommandHandler<CreateUserCommand, VoidResult> {

	private UserService userService;

	@Autowired
	public CreateUserCommandHandler(UserService userService) {
		this.userService = userService;
	}

	@Override
	public VoidResult execute(CreateUserCommand command) {
		return userService.registerNewUser(command);
	}

	@Override
	public Class getCommandType() {
		return CreateUserCommand.class;
	}
	
}
