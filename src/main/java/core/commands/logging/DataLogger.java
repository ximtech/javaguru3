package core.commands.logging;

import core.exceptions.ValidationException;
import core.operationresult.ResponseMessage;
import core.operationresult.Status;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DataLogger {
    private static final Logger LOGGER = LogManager.getLogger(DataLogger.class);

    public static void logErrorMessage(Exception exception, ResponseMessage responseMessage) {
        logException(responseMessage, exception.getClass().getSimpleName());
        exception.printStackTrace();
    }

    public static void logErrorMessage(ValidationException exception, ResponseMessage responseMessage) {
        logException(responseMessage, exception.getClass().getSimpleName());
    }

    public static void logRequestStart(Object request) {
        LOGGER.info(String.format("Started request %s", request.toString()));
    }

    public static void logRequestEnd(Object request, long endTime) {
        String className = request.getClass().getSimpleName();
        LOGGER.info(String.format("Request with name: %s finished in %d ms", className, endTime));
    }

    public static void logMessageSendingToQueue(Object request, String queueName) {
        String className = request.getClass().getSimpleName();
        LOGGER.info("Sending mail request={} to destination queue={}",className, queueName);
    }

    private static void logException(ResponseMessage responseMessage, String exceptionClassName) {
        Status responseStatus = responseMessage.getStatus();
        String message = responseMessage.getMessage();
        LOGGER.error(String.format("%1$s. Status: %2$s Message: %3$s", exceptionClassName, responseStatus, message));
    }
}
