package core.services;

import core.commands.DomainCommand;
import core.commands.logging.DataLogger;
import core.commands.validation.CommandValidator;
import core.operationresult.ProcessingResult;
import core.services.interfaces.command.CommandExecutor;
import core.services.interfaces.command.DomainCommandHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
class CommandExecutorImpl implements CommandExecutor {
    private Map<Class, DomainCommandHandler> commandHandlersMap = new HashMap<>();

    private List<DomainCommandHandler> commandHandlers;

    @Autowired
    public CommandExecutorImpl(List<DomainCommandHandler> commandHandlers) {
        this.commandHandlers = commandHandlers;
    }

    @PostConstruct
    public void init() {
        commandHandlers.forEach((DomainCommandHandler service) -> {
            Class domainCommandClass = service.getCommandType();
            commandHandlersMap.put(domainCommandClass, service);
        });
    }

    @Transactional
    public <T extends ProcessingResult> T execute(DomainCommand<T> domainCommand) {
        long start = System.currentTimeMillis();
        DataLogger.logRequestStart(domainCommand);
        CommandValidator.validateCommand(domainCommand);

        T processingResult = proceed(domainCommand);

        long end = System.currentTimeMillis();
        long endTime = end - start;
        DataLogger.logRequestEnd(domainCommand, endTime);

        return processingResult;
    }

    @SuppressWarnings("unchecked")
    private <T extends ProcessingResult> T proceed(DomainCommand<T> domainCommand) {
        DomainCommandHandler service = commandHandlersMap.get(domainCommand.getClass());
        if(service != null) {
            return (T)service.execute(domainCommand);
        } else {
            throw new IllegalArgumentException("Unknown domain command! " + domainCommand.toString());
        }
    }

}
