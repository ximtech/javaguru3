package core.services.interfaces.clients;

import core.commands.VoidResult;
import core.commands.users.CreateUserCommand;
import core.commands.users.GetUserCommand;
import core.commands.users.GetUserResult;


public interface UserService {

    VoidResult registerNewUser(CreateUserCommand command);

    GetUserResult getUser(GetUserCommand command);

}
