package core.services.interfaces.clients;

import core.database.entities.User;

public interface ClientFactory {

    User create(String login, String password);

}
