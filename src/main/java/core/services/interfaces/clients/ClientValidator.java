package core.services.interfaces.clients;

public interface ClientValidator {

    void validate(String login, String password);

}
