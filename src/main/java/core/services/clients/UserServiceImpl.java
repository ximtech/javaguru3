package core.services.clients;

import core.commands.VoidResult;
import core.commands.users.CreateUserCommand;
import core.commands.users.GetUserCommand;
import core.commands.users.GetUserResult;
import core.database.entities.User;
import core.database.factories.UserFactory;
import core.database.interfaces.UserRepository;
import core.dto.UserDTO;
import core.dto.utils.DTOMappingUtils;
import core.exceptions.ValidationException;
import core.operationresult.ResponseMessage;
import core.services.interfaces.clients.UserService;
import core.mq.interfaces.mail.EmailProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userDAO;
    private EmailProducer emailProducer;

    @Autowired
    public UserServiceImpl(UserRepository userDAO, EmailProducer emailProducer) {
        this.userDAO = userDAO;
        this.emailProducer = emailProducer;
    }

    @Override
    public VoidResult registerNewUser(CreateUserCommand command) {
        validateThatUserNotAlreadyExists(command.getEmail());
        User user = UserFactory.createCustomerUser(
                command.getFirstName(),
                command.getLastName(),
                command.getPassword(),
                command.getEmail());
        userDAO.save(user);
        emailProducer.sendWelcomeMailToUser(
                command.getFirstName(),
                command.getLastName(),
                command.getEmail());
        return new VoidResult();
    }

    @Override
    public GetUserResult getUser(GetUserCommand command) {
        GetUserResult result = new GetUserResult();
        Optional<User> optionalUser = userDAO.getUserByIdAndDeletedAndEnabled(command.getId(), false, true);

        if (optionalUser.isPresent()) {
            UserDTO userDTO = DTOMappingUtils.constructUserDtoFromEntity(optionalUser.get());
            result.setUser(userDTO);
            return result;
        }
        result.setResponseMessage(ResponseMessage.notFound());
        return result;
    }

    private void validateThatUserNotAlreadyExists(String email) {
        Optional<User> optionalUser = userDAO.getUserByUsername(email);
        if (optionalUser.isPresent()) {
            throw new ValidationException(ResponseMessage.businessError("User with username: " + email + " already exist"));
        }
    }
}
