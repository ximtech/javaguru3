package core.database.factories;

import core.database.UserRole;
import core.database.entities.User;

public class UserFactory {

    public static User createCustomerUser(String firstName,
                                          String lastName,
                                          String password,
                                          String emailAddress) {
        return new User(false, false, true, password, emailAddress, emailAddress, firstName, lastName, UserRole.ROLE_USER);
    }
}
