package core.database.interfaces;

import core.database.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> getUserByUsername(String username);

    Optional<User> getUserByIdAndDeletedAndEnabled(Long id, boolean deleted, boolean enabled);
}
