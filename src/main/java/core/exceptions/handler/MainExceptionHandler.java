package core.exceptions.handler;

import core.commands.logging.DataLogger;
import core.exceptions.ValidationException;
import core.operationresult.ResponseMessage;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestControllerAdvice
public class MainExceptionHandler extends ResponseEntityExceptionHandler {

    @ResponseBody
    @ExceptionHandler(ValidationException.class)
    @ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
    public ResponseMessage handleInvalidRequest(ValidationException exception, ServletWebRequest request) {
        ResponseMessage responseMessage = exception.getResponseMessage();
        DataLogger.logErrorMessage(exception, responseMessage);
        return responseMessage;
    }

    @ResponseBody
    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseMessage handleInvalidRequest(Exception exception, ServletWebRequest request) {
        ResponseMessage responseMessage = ResponseMessage.internalException();
        DataLogger.logErrorMessage(exception, responseMessage);
        return responseMessage;
    }
}
