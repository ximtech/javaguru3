package core.exceptions;

import core.operationresult.ResponseMessage;

public class ValidationException extends RuntimeException {
    private ResponseMessage responseMessage;

    public ValidationException(ResponseMessage responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ResponseMessage getResponseMessage() {
        return responseMessage;
    }
}
