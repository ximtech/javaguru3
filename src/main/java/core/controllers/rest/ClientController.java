package core.controllers.rest;

import core.commands.VoidResult;
import core.commands.users.CreateUserCommand;
import core.commands.users.GetUserCommand;
import core.commands.users.GetUserResult;
import core.services.interfaces.command.CommandExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "/api")
public class ClientController {//TODO implement scheduler

    private CommandExecutor commandExecutor;

    @Autowired
    public ClientController(CommandExecutor commandExecutor) {
        this.commandExecutor = commandExecutor;
    }

    @PostMapping(value="/registerNewUser")
    public Mono<VoidResult> create(CreateUserCommand command) {
        return Mono.fromCallable(() -> commandExecutor.execute(command));
    }

    @GetMapping(value="/receiveUser")
    public Mono<GetUserResult> getUser(GetUserCommand command) {
        return Mono.fromCallable(() -> commandExecutor.execute(command));
    }

}
