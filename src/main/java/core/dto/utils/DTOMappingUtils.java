package core.dto.utils;

import core.database.entities.User;
import core.dto.UserDTO;

public class DTOMappingUtils {

    public static UserDTO constructUserDtoFromEntity(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setAccountLocked(user.isAccountLocked());
        userDTO.setDeleted(user.isDeleted());
        userDTO.setEnabled(user.isEnabled());
        userDTO.setPassword(user.getPassword());
        userDTO.setEmailAddress(user.getEmailAddress());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());
        userDTO.setRole(user.getRole());
        return userDTO;
    }
}
