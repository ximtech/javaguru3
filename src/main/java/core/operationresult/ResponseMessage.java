package core.operationresult;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.StringUtils;

import static com.fasterxml.jackson.annotation.JsonInclude.Include;
import static core.operationresult.DefaultMessages.*;

@JsonPropertyOrder({ "type", "status", "message", "fieldName" })
public class ResponseMessage {

    @JsonProperty
    private Type type;

    @JsonProperty
    private Status status;

    @JsonProperty
    private String message;
    
    @JsonInclude(Include.NON_NULL)
    private String fieldName;

    private ResponseMessage(Type type, Status status, String message) {
        this(type, status, message, null);
    }
    
    private ResponseMessage(Type type, Status status, String message, String fieldName) {
        this.status = status;
        this.type = type;
        this.fieldName = StringUtils.isBlank(fieldName) ? null : fieldName;
        this.message = fieldName != null ? String.format(message, fieldName) : message;
    }

    public static ResponseMessage success() {
        return success(MSG_DATA_SAVED);
    }

    public static ResponseMessage success(String message) {
        return new ResponseMessage(Type.SUCCESS, Status.OK, message);
    }

    public static ResponseMessage businessError(String message) {
        return businessError(message, null);
    }

    public static ResponseMessage businessError(String message, String fieldId) {
        return new ResponseMessage(Type.ERROR, Status.UNPROCESSABLE_ENTITY, message, fieldId);
    }

    public static ResponseMessage notFound() {
        return new ResponseMessage(Type.ERROR, Status.NOT_FOUND, MSG_DATA_NOT_FOUND);
    }

    public static ResponseMessage internalException() {
        return new ResponseMessage(Type.ERROR, Status.INTERNAL_SERVER_ERROR, MSG_ERR_DEFAULT);
    }

    public static ResponseMessage forbidden(String message) {
        return new ResponseMessage(Type.ERROR, Status.FORBIDDEN, message);
    }

    public static ResponseMessage unauthorized(String message) {
        return new ResponseMessage(Type.ERROR, Status.UNAUTHORIZED, message);
    }

    public Type getType() {
        return type;
    }

    public Status getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    public String getFieldName() {
        return fieldName;
    }
}
