package core.operationresult;

public class DefaultMessages {
    static final String MSG_DATA_SAVED = "Data is saved";
    static final String MSG_DATA_NOT_FOUND = "Requested data not found";
    static final String MSG_ERR_DEFAULT = "We are sorry, something went wrong." +
            "Please try again at a later time or contact technical support if the problem persists";

    public static final String MSG_BLANK_FIELD = "Mandatory field %s is blank";
    public static final String MSG_EMAIL_INVALID = "Email address is incorrect";
    public static final String MSG_ID_INVALID = "Id is incorrect";
    public static final String MSG_WEAK_PASSWORD = "You entered a weak password. " +
            "Please use a combination of 8 symbols which must include digits and " +
            "both uppercase and lowercase letters of the Latin alphabet";

    public static final String FIELD_ID_ID= "Id";
    public static final String FIELD_ID_FIRST_NAME = "First Name";
    public static final String FIELD_ID_LAST_NAME = "Last Name";
    public static final String FIELD_ID_PASSWORD = "Password";
    public static final String FIELD_ID_EMAIL = "Email";

}
