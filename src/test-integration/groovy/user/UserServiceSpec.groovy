package user

import core.Application
import core.commands.VoidResult
import core.commands.users.CreateUserCommand
import core.commands.users.GetUserCommand
import core.commands.users.GetUserResult
import core.database.UserRole
import core.database.entities.User
import core.database.interfaces.UserRepository
import core.exceptions.ValidationException
import core.mq.interfaces.mail.EmailProducer
import core.mq.mail.EmailProducerImpl
import core.operationresult.Status
import core.operationresult.Type
import core.services.clients.UserServiceImpl
import core.services.interfaces.clients.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.TestPropertySource
import spock.lang.Specification

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT

@DirtiesContext
@SpringBootTest(webEnvironment = RANDOM_PORT, classes = Application.class)
@TestPropertySource("classpath:test-application.properties")
class UserServiceSpec extends Specification {

    EmailProducer emailProducer
    UserService userService

    @Autowired
    UserRepository userDAO

    void setup() {
        emailProducer = Mock(EmailProducerImpl)
        userService = new UserServiceImpl(userDAO, emailProducer)
    }

    void "test registerNewUser() - should correctly register and validate new user"() {
        given:
        CreateUserCommand command = new CreateUserCommand()
        command.firstName = "firstName"
        command.lastName = "lastName"
        command.password = "12345anv"
        command.email = "other@email.com"

        when:
        VoidResult result = userService.registerNewUser(command)

        then: "should insert new user and return correct status"
       1 * emailProducer.sendWelcomeMailToUser("firstName", "lastName", "other@email.com")

        result.responseMessage.type == Type.SUCCESS
        Optional<User> user = userDAO.getUserByUsername("other@email.com")
        user.isPresent()

        when:
        userService.registerNewUser(command)

        then: "should throw error, because user name already exist"
        ValidationException exception = thrown(ValidationException)
        exception.responseMessage.type == Type.ERROR
        exception.responseMessage.status == Status.UNPROCESSABLE_ENTITY
    }

    void "test getUser() - should return user by id"() {
        given:
        GetUserCommand command = new GetUserCommand()
        command.id = 100L

        when:
        GetUserResult result = userService.getUser(command)

        then: "should find user by id"
        result.user != null
        result.user.firstName == "Stanislav"
        result.user.lastName == "Vodolagin"
        result.user.role == UserRole.ROLE_ADMIN
    }

}
